package com.nCinga;

public interface Department {
    void IncreaseProduction();
    void DecreaseProduction();
}
