package com.nCinga;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Cutting cutting=new Cutting(5);
        cutting.IncreaseProduction();
        cutting.DecreaseProduction();

        Sewing sewing=new Sewing(5);
        sewing.IncreaseProduction();
        sewing.DecreaseProduction();

        Packing packing=new Packing(10);
        packing.IncreaseProduction();
        packing.DecreaseProduction();
    }
}
