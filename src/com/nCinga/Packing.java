package com.nCinga;

public class Packing implements Department {
    int packingCount;
    public Packing(){
        packingCount = 0;
    }
    public Packing(int count){
        packingCount = count;
    }
    @Override
    public void IncreaseProduction() {
        packingCount+=5;
        System.out.println(packingCount);
    }
    public void DecreaseProduction() {
        if (packingCount>5){
            packingCount-=5;
            //System.out.println(packingCount);
        }
        else{
            packingCount =0;
            //System.out.println(packingCount);
        }
        System.out.println(packingCount);
    }
}