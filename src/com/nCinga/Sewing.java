package com.nCinga;

public class Sewing implements Department {
    int sewingCount;
    public Sewing(){
        sewingCount = 0;
    }
    public Sewing(int count){
        sewingCount = count;
    }

    @Override
    public void IncreaseProduction() {
        sewingCount+=1;
        System.out.println(sewingCount);
    }
    public void DecreaseProduction(){
        if (sewingCount>0){
            sewingCount-=1;

        }
        else{
            sewingCount=0;

        }
        System.out.println(sewingCount);
    }
}